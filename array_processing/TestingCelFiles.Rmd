Checking CEL files for CressExpress 2.0
==================

This document describe reviewing CEL files corresponding to arrays from Gene Expression Omnibus (GEO). We'll be using these data to create a new release of the CressExpress database and web-based co-expression analysis tool.

Background
---

Nikhil Dahake (Loraine lab Graduate Research Assistant) used eUtils and custom scripts to harvest around 9000+ CEL file from the ATH1/GPL198 array from Affymetrix. Scripts he created are in the pysrc directory. 

He created a file named data/gsm2cel.tsv that maps GSM ids onto CEL file names. I wrote some code that attempted to process each of the CEL files in the document. Using this code, I wrote a document data/tested.tsv that reports the outcome of this testing.

In this document, I'll investigate how many CEL files failed to process, how many GSM ids have multiple CEL files, and then build a listing of CEL files we'll process and import into the CressExpress database.

Read the data
---

```{r}
d=read.delim('data/tested.tsv',header=T,sep='\t',as.is=T)
names(d)
```

This data set had `r nrow(d)` rows and `r length(unique(d$gsm))` unique gsm ids. There were `r length(unique(d$cel))` unique CEL files.

Thus, there were no duplicate CEL files. However, there were some GSM ids that mapped onto multiple CEL files.

Let's investigate the GSMs that mapped onto multiple CEL files:


```{r}
dups=unique(d$gsm[duplicated(d$gsm)])
```

There were `r length(dups)` GSM ids with duplicate CEL files. Let's look them over:

```{r}
d[d$gsm%in%dups,]
```

I don't know why these GSMs are represented multiple times. Maybe they are true biological replicates? Or maybe they are duplicate scans of the same array? This is weird. 

I don't want to model multiple CEL files per GSM id in the database. Instead, I'll select CEL files that (a) could be processed (test is True) and (b) are listed first in the list.

Obviously (b) is somewhat arbitrary. However, the listing of CEL files looks like it is in some kind of order, i.e., CEL file names containing "_1" typically appear before names containg "_2", suggesting there is some kind of logic at work here. Also, since the data file I'm using is version-controlled, it will be possible to look up the CEL file used if needed. Moreover, this issue affects only 24 GSM ids. 

Investigate non-processable GSM/CEL files
----

There were `r sum(!d$test)` CEL files that could not be processed. 

Let's investigate these:

```{r}
d[!d$test,c('gsm','cel')]
```

Looks like some of these may be from the same GSE, while others are flagged as "truncated" in the names of the CEL files. 

We should look into whether there are some GSE sets that have multiple unreadable CEL files. I asked Nikhil for a data file that maps GSM onto GSE ids, which would allow me to investigate this. For now, let's move on.

Making data file with usable GSM/CEL files
----

We can't use unprocessable CEL files, so let's eliminate those.

```{r}
d.ok=d[d$test,]
```

After getting rid of unusable CEL files, we were left with `r length(unique(d.ok$gsm))` GSM ids.

Now, let's remove duplicates. 

```{r}
d.nodups=d.ok[!duplicated(d.ok$gsm),]
```

After removing duplicates, there were `r nrow(d.nodups)` with `r length(unique(d$gsm))` unique GSM ids.

Write out the data:

```{r}
outf='data/r4.cleaned.tsv'
write.table(d.nodups[,c('gsm','cel')],file=outf,row.names=F,sep='\t',quote=F)
```

And we're done!

