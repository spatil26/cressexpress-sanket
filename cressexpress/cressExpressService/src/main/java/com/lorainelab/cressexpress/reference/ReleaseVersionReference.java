/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.lorainelab.cressexpress.reference;

import com.google.common.base.Splitter;
import com.google.common.collect.ImmutableBiMap;
import com.google.common.collect.ImmutableList;
import com.google.common.collect.ImmutableMap;
import com.lorainelab.cressexpress.controller.ReleaseVersionController;
import com.lorainelab.cressexpress.model.ReleaseVersion;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import javax.annotation.PostConstruct;
import javax.ejb.ConcurrencyManagement;
import static javax.ejb.ConcurrencyManagementType.BEAN;
import javax.ejb.Singleton;
import javax.ejb.Startup;
import javax.inject.Inject;

/**
 *
 * @author dcnorris
 */
@Startup
@ConcurrencyManagement(BEAN)
@Singleton
public class ReleaseVersionReference {

    @Inject
    private ReleaseVersionController releaseVersionController;

    private ImmutableList<ReleaseVersion> allReleaseVersions;

    private Map<ReleaseVersion, List<String>> releaseOrderReference;

    @PostConstruct
    public void setup() {
        allReleaseVersions = ImmutableList.<ReleaseVersion>builder().addAll(releaseVersionController.findReleaseVersionEntities()).build();
        ImmutableMap.Builder<ReleaseVersion, List<String>> releaseOrderReferenceBuilder = ImmutableBiMap.<ReleaseVersion, List<String>>builder();
        for (ReleaseVersion rv : allReleaseVersions) {
            List<String> microArrayOrder = new ArrayList<>();
            microArrayOrder.addAll(Splitter.on(',').trimResults().omitEmptyStrings().splitToList(rv.getArrayOrder()));
            releaseOrderReferenceBuilder.put(rv, microArrayOrder);
        }

        releaseOrderReference = releaseOrderReferenceBuilder.build();
    }

    public ImmutableList<ReleaseVersion> getAllReleaseVersions() {
        return allReleaseVersions;
    }

    public List<String> getDefaultMicroarrayOrder() {
        return releaseOrderReference.get(releaseVersionController.findReleaseVersionEntities().get(0));
    }

    public Map<ReleaseVersion, List<String>> getReleaseOrderReference() {
        return releaseOrderReference;
    }

}
