/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.lorainelab.cressexpress.reference;

import com.lorainelab.cressexpress.controller.MicroarrayController;
import com.lorainelab.cressexpress.model.Microarray;
import java.util.ArrayList;
import java.util.List;
import javax.annotation.PostConstruct;
import javax.ejb.ConcurrencyManagement;
import static javax.ejb.ConcurrencyManagementType.BEAN;
import javax.ejb.DependsOn;
import javax.ejb.Singleton;
import javax.ejb.Startup;
import javax.inject.Inject;

/**
 *
 * @author dcnorris
 */
@Startup
@ConcurrencyManagement(BEAN)
@DependsOn("ReleaseVersionReference")
@Singleton
public class MicroarrayDetailReference {

    @Inject
    private ReleaseVersionReference releaseVersionReference;

    @Inject
    private MicroarrayController microarrayController;

    private List<Microarray> defaultReleaseMicroarrays;

    @PostConstruct
    public void setup() {
        defaultReleaseMicroarrays = new ArrayList<>();
        for (String microarrayName : releaseVersionReference.getDefaultMicroarrayOrder()) {
            defaultReleaseMicroarrays.add(microarrayController.findByName(microarrayName));
        }
    }

    public List<Microarray> getDefaultReleaseMicroarrays() {
        return defaultReleaseMicroarrays;
    }

}
