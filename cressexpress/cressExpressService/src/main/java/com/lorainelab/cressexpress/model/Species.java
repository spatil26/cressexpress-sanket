/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.lorainelab.cressexpress.model;

import java.io.Serializable;
import java.util.Set;
import javax.persistence.Basic;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlTransient;

/**
 *
 * @author dcnorris
 */
@Entity
@Table(name = "species")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "Species.findAll", query = "SELECT s FROM Species s"),
    @NamedQuery(name = "Species.findById", query = "SELECT s FROM Species s WHERE s.id = :id"),
    @NamedQuery(name = "Species.findByGenus", query = "SELECT s FROM Species s WHERE s.genus = :genus"),
    @NamedQuery(name = "Species.findBySpecies", query = "SELECT s FROM Species s WHERE s.species = :species"),
    @NamedQuery(name = "Species.findByVariety", query = "SELECT s FROM Species s WHERE s.variety = :variety")})
public class Species implements Serializable {
    private static final long serialVersionUID = 1L;
    @Id
    @Basic(optional = false)
    @Column(name = "id")
    private Integer id;
    @Basic(optional = false)
    @Column(name = "genus")
    private String genus;
    @Basic(optional = false)
    @Column(name = "species")
    private String species;
    @Column(name = "variety")
    private String variety;
    @Column(name = "taxId")
    private Integer taxId;
    @OneToMany(cascade = CascadeType.ALL, mappedBy = "species")
    private Set<Gene> genes;

    public Species() {
    }

    public Species(Integer id) {
        this.id = id;
    }

    public Species(Integer id, String genus, String species) {
        this.id = id;
        this.genus = genus;
        this.species = species;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getGenus() {
        return genus;
    }

    public void setGenus(String genus) {
        this.genus = genus;
    }

    public String getSpecies() {
        return species;
    }

    public void setSpecies(String species) {
        this.species = species;
    }

    public String getVariety() {
        return variety;
    }

    public void setVariety(String variety) {
        this.variety = variety;
    }

    public Integer getTaxId() {
        return taxId;
    }

    public void setTaxId(Integer taxId) {
        this.taxId = taxId;
    }

    @XmlTransient
    public Set<Gene> getGenes() {
        return genes;
    }

    public void setGenes(Set<Gene> genes) {
        this.genes = genes;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (id != null ? id.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof Species)) {
            return false;
        }
        Species other = (Species) object;
        if ((this.id == null && other.id != null) || (this.id != null && !this.id.equals(other.id))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "com.lorainelab.cressdbbuilder.Species[ id=" + id + " ]";
    }
    
}
