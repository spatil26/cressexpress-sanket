/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.lorainelab.cressexpress.model;

import java.io.Serializable;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author dcnorris
 */
@Entity
@Table(name = "microarrayAttribute")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "MicroarrayAttribute.findAll", query = "SELECT m FROM MicroarrayAttribute m"),
    @NamedQuery(name = "MicroarrayAttribute.findByNamespace", query = "SELECT m FROM MicroarrayAttribute m WHERE m.namespace = :namespace"),
    @NamedQuery(name = "MicroarrayAttribute.findByName", query = "SELECT m FROM MicroarrayAttribute m WHERE m.name = :name"),
    @NamedQuery(name = "MicroarrayAttribute.findByValue", query = "SELECT m FROM MicroarrayAttribute m WHERE m.value = :value"),
    @NamedQuery(name = "MicroarrayAttribute.findById", query = "SELECT m FROM MicroarrayAttribute m WHERE m.id = :id")})
public class MicroarrayAttribute implements Serializable {

    private static final long serialVersionUID = 1L;

    @Column(name = "namespace")
    private String namespace;

    @Basic(optional = false)
    @Column(name = "name")
    private String name;

    @Basic(optional = false)
    @Column(name = "value")
    private String value;

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Basic(optional = false)
    @Column(name = "id")
    private Integer id;

    @JoinColumn(name = "microarray", referencedColumnName = "id")
    @ManyToOne(optional = false)
    private Microarray microarray;

    public MicroarrayAttribute() {
    }

    public MicroarrayAttribute(Integer id) {
        this.id = id;
    }

    public MicroarrayAttribute(Integer id, String name, String value) {
        this.id = id;
        this.name = name;
        this.value = value;
    }

    public String getNamespace() {
        return namespace;
    }

    public void setNamespace(String namespace) {
        this.namespace = namespace;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getValue() {
        return value;
    }

    public void setValue(String value) {
        this.value = value;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public Microarray getMicroarray() {
        return microarray;
    }

    public void setMicroarray(Microarray microarray) {
        this.microarray = microarray;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (id != null ? id.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof MicroarrayAttribute)) {
            return false;
        }
        MicroarrayAttribute other = (MicroarrayAttribute) object;
        if ((this.id == null && other.id != null) || (this.id != null && !this.id.equals(other.id))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "com.lorainelab.cressdbbuilder.MicroarrayAttribute[ id=" + id + " ]";
    }

}
