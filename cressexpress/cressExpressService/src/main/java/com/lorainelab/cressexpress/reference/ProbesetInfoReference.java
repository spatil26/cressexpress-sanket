/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.lorainelab.cressexpress.reference;

import com.google.common.collect.ImmutableSet;
import com.lorainelab.cressexpress.controller.ProbesetController;
import java.util.Map;
import java.util.Set;
import java.util.TreeMap;
import javax.annotation.PostConstruct;
import javax.ejb.ConcurrencyManagement;
import static javax.ejb.ConcurrencyManagementType.BEAN;
import javax.ejb.Singleton;
import javax.ejb.Startup;
import javax.inject.Inject;

/**
 *
 * @author dcnorris
 */
@Startup
@ConcurrencyManagement(BEAN)
@Singleton
public class ProbesetInfoReference {

    @Inject
    private ProbesetController probesetController;

    private TreeMap<String, Integer> probesetIdMap;
    private Map<String, Integer> probesetIndexReference;
    private Set<String> probesetNames;

    @PostConstruct
    public void setup() {
        //TODO this will ultimately need to be filtered based on release, so we will create a map instead of a single list
        probesetIdMap = probesetController.getProbesetIdMap();
        probesetNames = ImmutableSet.<String>builder().addAll(probesetIdMap.keySet()).build();
        probesetIndexReference = new TreeMap<>();
        int i = 0;
        for (String name : probesetNames) {
            probesetIndexReference.put(name, i);
            i++;
        }
    }

    public Map<String, Integer> getProbesetIdMap() {
        return probesetIdMap;
    }

    public Set<String> getProbesetNames() {
        return probesetNames;
    }

    public Map<String, Integer> getProbesetIndexReference() {
        return probesetIndexReference;
    }

}
