package com.lorainelab;

import java.io.IOException;
import org.junit.Before;

/**
 *
 * @author dcnorris
 */
public class RserveExample {

//    List<String> microarrayOrder;
//    List<String> microarraySelections;
//    List<String> probesetSelections;
//    double plcRsquaredThreshold = 0.36;
//    String expressionFlatFile = "/home/dcnorris/NetBeansProjects/cressExpressFlatFileDump/exprRowData.txt";

    @Before
    public void setup() throws IOException {
//        String microarrayOrderSource = CharStreams.toString(new InputStreamReader(this.getClass().getClassLoader().getResourceAsStream("microarrayOrder.txt"), Charsets.UTF_8));
//        String microarraySelectionsSource = CharStreams.toString(new InputStreamReader(this.getClass().getClassLoader().getResourceAsStream("microarraySelections.txt"), Charsets.UTF_8));
//        String probesetSelectionsSource = CharStreams.toString(new InputStreamReader(this.getClass().getClassLoader().getResourceAsStream("probesetSelections.txt"), Charsets.UTF_8));
//        microarrayOrder = Splitter.on(',').trimResults().omitEmptyStrings().splitToList(microarrayOrderSource);
//        probesetSelections = Splitter.on(',').trimResults().omitEmptyStrings().splitToList(probesetSelectionsSource);
//        microarraySelections = Splitter.on(',').trimResults().omitEmptyStrings().splitToList(microarraySelectionsSource);
    }

//    @Test
//    public void correlationAnalysis() throws ScriptException, IOException, RserveException, REXPMismatchException, REngineException {
//        RConnection engine = new RConnection();
//        engine.assign("microarrayOrder", microarrayOrder.toArray(new String[0]));
//        engine.assign("baitProbesets", probesetSelections.toArray(new String[0]));
//        engine.assign("microarraySelections", microarraySelections.toArray(new String[0]));
//        engine.assign("fname", expressionFlatFile);
//        engine.eval("d=read.delim(fname,header=F,as.is=T,sep=',')");
//        engine.eval("row.names(d)=d[,1]");
//        engine.eval("d=d[,-1]");
//        engine.eval("names(d)=microarrayOrder");
//        engine.eval("stopAt=nrow(d)");
//        engine.eval("results=numeric(stopAt*length(baitProbesets))");
//        engine.eval("resultsIndex=1");
//        engine.eval("for (otherProbeset in row.names(d)[1:stopAt]) {"
//                + "  x=unlist(d[otherProbeset,microarraySelections,drop=T]);"
//                + "  for (baitProbeset in baitProbesets) {"
//                + "    y=unlist(d[baitProbeset,microarraySelections,drop=T]);"
//                + "    results[resultsIndex]=cor(x,y);"
//                + "    resultsIndex=resultsIndex+1;"
//                + "  }"
//                + "}");
//        engine.eval("print(results[1])");
//        engine.eval("print(resultsIndex)");
//        engine.eval("print(results[resultsIndex-1])");
//    }

}
